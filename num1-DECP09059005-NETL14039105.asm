# Travail pratique 1
# Dans le cadre du cours INF4170: Architecture des ordinateurs
# Par
#	* Pier-Olivier Decoste (DECP09059005)
#	* Lou-Gomes Neto (NETL14039105)
# Question numero 1
###############################################################

.globl swap, getLeftChildIndex, getRightChildIndex 
.text

# Debut du tableau = $s0
# Longueur du tableau = $s1

	# parametres int i et j = $a0 et $a1
	# debut du tableau = $s0
	swap: 	 
		sll $t0, $a0, 2
		add $t0, $t0, $s0
		sll $t1, $a1, 2
		add $t1, $t1, $s0
		
		# tab[i] = tab[j]
		# tab[j] = tab[i]
		lw $t2, 0($t0)
		lw $t3, 0($t1)
		sw $t3, 0($t0)
		sw $t2, 0($t1) 
		# return void
		jr $ra

	# parametre int index = $a0
	# return $v0
	getLeftChildIndex: 	
		sll  $v0, $a0, 1   
		addi  $v0, $v0, 1
		jr $ra
	
	# parametres int index = $a0
	# return $v0	
	getRightChildIndex: 
		sll  $v0, $a0, 1   
	 	addi  $v0, $v0, 2
	 	jr $ra 

