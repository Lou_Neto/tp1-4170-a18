# Travail pratique 1
# Dans le cadre du cours INF4170: Architecture des ordinateurs
# Par
#	* Pier-Olivier Decoste (DECP09059005)
#	* Lou-Gomes Neto (NETL14039105)
# Question numero 2
###############################################################

.data
	msgNb:		.asciiz  "Entrez la longeur du tableau : "
	msgTab:		.asciiz  "\nEntrez les elements du tableau : "
.text

# MAIN --------------------------------------------------------

	main:	
			# print invite pour entrer la longeur du tableau
			li $v0, 4
			la $a0, msgNb
			syscall
			
			# lire integer et stocker dans $s1
			li $v0, 5
			syscall
			move $s1, $v0
			
			# print invite pour entrer les elements du tableau
			li $v0, 4
			la $a0, msgTab
			syscall
			
			# Initialisation du tableau et for loop
			lui $s0, 0x1004
			ori $s0, 0x0000
			addi $s2, $zero, 0 # $s2 : int i = 0
			add $t1, $zero, $s1 # $t1 = fin tab ($s1)
	fillLoop: 	
			# i < tabLen 
			slt $t0, $s2, $t1
			beq $t0, $zero, done
			
			# lire integer
			li $v0, 5
			syscall
			move $t2, $v0
			
			# stocker dans tableau
			sll $t0, $s2, 2
			add $t0, $t0, $s0
			sw $t2, 0($t0)
			
			# ++i et continue
			addi $s2, $s2, 1
			j fillLoop
		
			# fin fillLoop
	done:		
			# Sort
			# Debut tableau = $s0
			# Tab len = $s1
			jal  sort

	finPgm:	        
			# fin programme
	 		li $v0, 10	
			syscall	

# FONCTIONS ----------------------------------------------------------

# public void sort()

	sort: 	
			# store adresse	du retour au main
			addi $sp, $sp, -4
        	sw   $ra, 0($sp) 
        	move $s6, $ra
        	
        	# $s4 = int n = a.length - 1
			# $s5 = int i = (n-1)/2
        	addi $s4, $s1, -1
      	    addi $s5, $s4, -1 
      		srl  $s5, $s5, 1  
      			
	for:		
			# if i < 0 then passe au while
			blt  $s5, $zero, while
			# fixheap($s5, $s4)
			move $a0, $s5
      		move $a1, $s4 
      		jal  fixheap
      		# --i
      		addi $s5, $s5, -1 
      		j for	
      		
	while:	
			# $s4 = int n = a.length - 1	
			# if n <= 0 then passe a fin
			ble  $s4, $zero, fin
			# Swap(0, $s4)
      		li   $a0, 0
      		move $a1, $s4
      		jal  swap
      		# --n
      		addi $s4, $s4, -1
      		# fixheap(0, $s4)
      		li   $a0, 0
      		move $a1, $s4
      		jal  fixheap
      		j while
        
	fin:  	
			# Reprendre l'adresse de retour au main sur la pile	
        	lw $ra, 0($sp)
        	addi $sp, $sp, 4
        	move $ra, $s6
        	# Return au main
        	jr $ra

# void fix heap 
			# int rootIndex = $a0 , int lastIndex = $a1
	fixheap:       
			# store adresse	du retour a sort
			addi $sp,$sp,-4
			sw   $ra,4($sp)
			
			# int rootValue = a[rootIndex]
			# $t0 = rootIndex
			# $t1 = rootValue
			move $t0, $a0 
			sll  $t0, $t0, 2
			add  $t0, $t0, $s0
			lw	 $t1, 0($t0)
			
			# t2 = int index = rootIndex
			move $t2, $a0 
			
	while1: 
	        # getLeftChildIndex($t2)
			move $a0, $t2
			jal  getLeftChildIndex
			# $t3 = int childIndex = return 2 * index + 1;
			move $t3, $v0 
			
			# if childIndex > lastIndex then fin
			bgt $t3, $a1, else
			
			# int rightChildIndex = getRightChildIndex(index);//2 * index + 2
			# getLeftChildIndex($t2)
			move $a0, $t2
			jal  getLeftChildIndex
			# $t4 = int childIndex = return 2 * index + 1;
			move $t4, $v0 
			
            # if (rightChildIndex <= lastIndex
			bgt $t4, $a1, else
			# && a[rightChildIndex] > a[childIndex])
			move $t9, $t4
			sll  $t9, $t9, 2
			add  $t9, $t9, $s0
			move $t8, $t3
			sll  $t8, $t8, 2
			add  $t8, $t8, $s0
			lw 	 $t7, 0($t9)
			lw	 $t6, 0($t8)
			ble  $t7, $t6, next
			# childIndex = rightChildIndex
			move $t3, $t4
			
	next: 	
			# if a[childIndex] <= rootValue then fin
			ble  $t6, $t1, else
			# a[index] = a[childIndex];
			move $t5, $t2
			sll  $t5, $t5, 2
			add  $t5, $t5, $s0
			sw   $t6, 0($t5)
			# index = childIndex
			move $t2, $t3
			j while1
				       
    else:	   
    		# more = false donc fin du while  
    		move $t5, $t2
			sll  $t5, $t5, 2
			add  $t5, $t5, $s0
			sw   $t1, 0($t5)
    		j exit   
      
    exit:   	
    		# Reprendre l'adresse de retour du main        
			lw $ra, 4($sp)
			addi $sp, $sp, 4
			jr $ra
       		        
